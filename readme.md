
# Duplicity for PI4 armv71 python 3.7

The build is done in a docker continer on the PI4,  the artefacts `./bin` and `./build/lib.....` from `setup.py build --force` are then exported as a archive tar.gz


# duplicity backup to gdrive

backup to a shared google myDrive (requires `gdrive:` backend support with shared drives WIP PR commit: xxxxxxxx )

To backup to a shared GOOGLE myDrive or a GOOGLE TEAM drive

a google service account is created and the required mydrive is shared to this service account
the service account credentials must be stored on the local machine. In this case they are stored to 
`~/.duplicity/${GOOGLE_SERVICE_ACCOUNT_NAME}_gserviceaccount.json`

The command to backup to a GOOGLE Team drive differs , requiring the duplicity command to have `rootFolderID` arguement be replaced with `driveID`

```bash
export GOOGLE_SERVICE_ACCOUNT_NAME=<SERVICE_ACCOUNT_NAME>
export GOOGLE_SERVICE_JSON_FILE="~/.duplicity/${GOOGLE_SERVICE_ACCOUNT_NAME}_gserviceaccount.json"

export BACKUP_SRC="<A PATH TO A SRC FOLDER"
export BACKUP_DEST_GDRIVE_SHARED_ROOT_FOLDER_ID="<SOME SHARED DRIVE FOLDER ID>" 
export BACKUP_DEST_GDRIVE_FOLDER_NAME="MY_PI4_BACKUP"

PYTHONPATH=./build/lib.linux-armv7l-3.7 poetry run bin/duplicity ${BACKUP_SRC}  gdrive://${GOOGLE_SERVICE_ACCOUNT_NAME}@${GOOGLE_SERVICE_ACCOUNT_NAME}.iam.gserviceaccount.com/${BACKUP_DEST_GDRIVE_FOLDER_NAME}/?rootFolderID=${BACKUP_DEST_GDRIVE_SHARED_ROOT_FOLDER_ID}

```

